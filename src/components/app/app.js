import React from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import TodoAdd from '../todo_add/index';

import './app.css';
// import './todo-add.css';


class App extends React.Component {
  state = {
    todos: [
      { id: 11, label: 'Drink vine', important: false, done: false },
      { id: 3, label: 'Have a dinner', important: true, done: true },
      { id: 2, label: 'Make Awesome', important: true, done: true },
    ],
    searchText: ''
  }

  

  onDelet = (id) => {
    this.setState ((oldState) => {
      const new_todos = oldState.todos.filter((todo) => todo.id !==id)
      return {todos: new_todos}
    })
  }


onImportand = (id) => {
  this.setState((oldState) => {

    const newTodos = oldState.todos.map((todo) => {
      if (todo.id ===id){
        return {...todo,important: !todo.important}
      } else {
        return {...todo}
      
      }
    
    })

    return {todos: newTodos}
  })
}


onDone = (id) => {
  this.setState((oldState) => {

    const newTodos = oldState.todos.map((todo) => {
      if (todo.id === id){
        return {...todo,done: !todo.done}
      } else {
        return {...todo}
      
      }
    
    })

    return {todos: newTodos}
  })
}
 onSearch = (text) => {
   this.setState({
     searchText: text
   })
 }

onSearchFilter = (filterText , todos) => {  
  const filteredTodos = todos.filter((todo) =>{
    let res = todo.label.toLowerCase().includes(filterText.toLowerCase())
    return res
  })
  
  return filteredTodos;
}

onAddNewTodo = (text) =>{

  let oldId  = this.state.todos.map((todo)=>todo.id)
  let id = oldId.sort((a,b)=>a-b)

  const newTodo = {
    id: id[id.length-1]+1,
    label: text,
    important: false,
    done: false
  }

  this.setState((oldState) => {
    return {
      todos: [...oldState.todos, newTodo]
    }
  })
}

  render() {
    const filterTodos = this.onSearchFilter(this.state.searchText, this.state.todos)
    const doneTodo = this.state.todos.filter((todo)=>todo.done)

    return (
      <div className="todo-app">
        <AppHeader toDo={this.state.todos.length} done={doneTodo.length} />

        <div className="top-panel d-flex">

          <SearchPanel onSearch ={this.onSearch}/>
          <ItemStatusFilter />
        </div>
        <TodoList todos={filterTodos} onDelete= {this.onDelet} onImportant ={this.onImportand} onDone={this.onDone}/>
        <TodoAdd onAddNewTodo ={this.onAddNewTodo} />
      </div>
    );
  };
}


export default App;
