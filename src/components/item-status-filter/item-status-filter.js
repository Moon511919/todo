import React from "react";
import { useEffect } from "react";
import { useState } from "react";

import "./item-status-filter.css";

const ItemStatusFilter = () => {

  const names = ["All", "Active", "Done"]; 

  const [text, setText] = useState("All"); //All, Active, Done // hooks useState()
  const [users, setUsers] = useState([]); //All, Active, Done // hooks useState()

  const btnText = (e) => {
    setText(e.target.innerText);
  };

  useEffect(() => {
    getdata()

  });

  const getdata = async () => {
    const req = await fetch("https://jsonplaceholder.typicode.com/users");
    const users = await req.json()
    setUsers(users)

  };

  return (
    <div className="btn-group">
      {names.map((btn, i) => {
        const clazz =
          text === btn ? "btn btn-info" : "btn btn-outline-secondary";

        return (
          <button
            key={i}
            type="button"
            className={clazz}
            onClick={(e) => btnText(e)}
          >
            {btn}
          </button>
        );
      })}

      {/* <button type="button" className="btn btn-info">
        All
      </button>
      <button type="button" className="btn btn-outline-secondary">
        Active
      </button>
      <button type="button" className="btn btn-outline-secondary">
        Done
      </button> */}
    </div>
  );
};

export default ItemStatusFilter;
