import React from "react";

class TodoAdd extends React.Component {
  state = {
    text: "",
  };

  todoAdd = (event) => {
    event.preventDefault();
    this.props.onAddNewTodo(this.state.text);
    this.setState({ text: "" });
  };
  componentDidMount() {
      this.getdata()
  }

  getdata = async () => {
    const req = await fetch("https://jsonplaceholder.typicode.com/todos/1");
    const res  = await req.json()
    console.log("res: TodoAdd ", res);
  };

  render() {

    return (
      <form onSubmit={this.todoAdd}>
        <input
          onChange={(event) => this.setState({ text: event.target.value })}
          value={this.state.text}
          type="text"
        />
        <input type="submit" value="submit" />
      </form>
    );
  }
}

export default TodoAdd;
